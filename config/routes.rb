Rails.application.routes.draw do
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    namespace 'api' do
        get '/health/check' => 'health_check#index'

        namespace 'v1' do
            get '/stores' => 'stores#find_all'
            get '/stores/:store_id' => 'stores#find_by_id'
            post '/stores' => 'stores#create'
            patch '/stores/:store_id' => 'stores#update'
            delete '/stores/:store_id' => 'stores#delete'

            get '/products' => 'products#find_all'
            get '/products/:product_id' => 'products#find_by_id'
            post '/products' => 'products#create'
            patch '/products/:product_id' => 'products#update'
            delete '/products/:product_id' => 'products#delete'

            get '/stores/:store_id/products' => 'stores_products#find_by_store'
            get '/stores/:store_id/products/:product_id' => 'stores_products#find_by_store_and_product'
            post '/stores/:store_id/products' => 'stores_products#create'
            patch '/stores/:store_id/products/:product_id' => 'stores_products#update'
            delete '/stores/:store_id/products/:product_id' => 'stores_products#delete'
        end
    end
end
