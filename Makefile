.PHONY: build
build:
	docker-compose build

.PHONY: up
up:
	docker-compose up -d

.PHONY: install
install:
	docker-compose exec nexaas bundle install

.PHONY: stop
stop:
	docker-compose stop

.PHONY: down
down:
	docker-compose down

.PHONY: migrate
migrate:
	docker-compose exec nexaas rails db:migrate
	docker-compose exec nexaas rails db:migrate RAILS_ENV=test

.PHONY: seed
seed:
	docker-compose exec nexaas rails db:seed

.PHONY: attach
attach:
	docker-compose exec nexaas bash

.PHONY: logs
logs:
	docker-compose logs -f

.PHONY: test
test:
	docker-compose exec nexaas rspec --format d

.PHONY: unit-test
unit-test:
	docker-compose exec nexaas rspec --format d --pattern spec/unit/**/*_spec.rb

.PHONY: integration-test
integration-test:
	docker-compose exec nexaas rspec --format d --pattern spec/integration/**/*_spec.rb
