FROM ruby:2.5

RUN echo "deb http://deb.debian.org/debian jessie main" > /etc/apt/sources.list
RUN echo "deb http://deb.debian.org/debian stable-updates main" >> /etc/apt/sources.list

RUN echo "deb http://repo.mysql.com/apt/debian/ jessie mysql-5.7" > /etc/apt/sources.list.d/mysql.list
RUN echo "deb-src http://repo.mysql.com/apt/debian/ jessie mysql-5.7" >> /etc/apt/sources.list.d/mysql.list

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8C718D3B5072E1F5

RUN apt-get update && \
    apt-get install s3cmd \
        libv8-dev \
        libqt4-dev \
        libqtwebkit-dev \
        libncurses5 \
        libtinfo5 \
        mysql-community-client \
        mysql-client \
        -y --allow-remove-essential && \
    apt-get clean

RUN mkdir /nexaas
WORKDIR /nexaas
COPY Gemfile /nexaas/Gemfile
COPY Gemfile.lock /nexaas/Gemfile.lock
RUN bundle install
COPY . /nexaas

COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]