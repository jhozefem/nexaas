class UpdatedTransformer
    def transform(item)
        {
            status: 'Updated',
            data: item.to_json
        }
    end
end
