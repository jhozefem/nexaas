class GetManyTransformer
    def transform(items)
        {
            total: items.count,
            data: items.map { |item| item.to_json }
        }
    end
end
