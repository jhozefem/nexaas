class NotFoundTransformer
    def transform(type = nil)
        if !type
            type = 'Item'
        end

        {
            status: 'NotFoundError',
            message: "#{type} not found"
        }
    end
end
