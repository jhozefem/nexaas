class ConflictTransformer
    def transform
        {
            status: 'ConflictError',
            message: 'Item already exists'
        }
    end
end
