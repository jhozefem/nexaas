class UnprocessableEntityTransformer
    def transform(errors = nil)
        {
            status: 'UnprocessableEntityError',
            message: 'Invalid data',
            errors: errors
        }
    end
end
