class CreatedTransformer
    def transform(item)
        {
            status: 'Created',
            data: item.to_json
        }
    end
end
