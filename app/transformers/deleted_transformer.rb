class DeletedTransformer
    def transform(item)
        {
            status: 'Deleted',
            data: item.to_json
        }
    end
end
