class StoreProduct < ApplicationRecord
    validates :quantity, presence: true, numericality: { only_integer: true }

    belongs_to :store, :foreign_key => :store_id
    belongs_to :product, :foreign_key => :product_id

    def to_json
        {
            id: self.id,
            store_id: self.store_id,
            product_id: self.product_id,
            quantity: self.quantity
        }
    end
end
