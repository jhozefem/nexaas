class Store < ApplicationRecord
    validates :name, presence: true
    validates :address_address1, presence: true
    validates :address_zip_code, presence: true, format: { :with => /\A\d{8}\z/, :message => 'must contain 8 digits' }
    validates :address_country, presence: true
    validates :address_state, presence: true
    validates :address_city, presence: true

    has_many :store_products, dependent: :destroy

    def address=(value)
        if value != nil
            self.address_address1 = value[:address1]
            self.address_address2 = value[:address2]
            self.address_zip_code = value[:zip_code]
            self.address_country = value[:country]
            self.address_state = value[:state]
            self.address_city = value[:city]
        end
    end

    def to_json
        {
            id: self.id,
            name: self.name,
            address: {
                address1: self.address_address1,
                address2: self.address_address2,
                zip_code: self.address_zip_code,
                country: self.address_country,
                state: self.address_state,
                city: self.address_city
            }
        }
    end
end
