class Product < ApplicationRecord
    validates :name, presence: true
    validates :price, presence: true, numericality: true

    has_many :store_products, dependent: :destroy

    def to_json
        {
            id: self.id,
            name: self.name,
            price: self.price
        }
    end
end
