module Api
    class HealthCheckController < ApplicationController
        def index
            render json: 'success', status: :ok
        end
    end
end
