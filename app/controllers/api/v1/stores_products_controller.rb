module Api
	module V1
		class StoresProductsController < ApplicationController
			STORE_MODEL_NAME = 'Store'
			STORE_PRODUCT_MODEL_NAME = 'StoreProduct'

			def find_by_store
				store_products = StoresProductsFindByStoreService.call(params[:store_id])
				render json: GetManyTransformer.new.transform(store_products), status: :ok
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(STORE_MODEL_NAME), status: :not_found
			end

			def find_by_store_and_product
				store_product = StoresProductsFindByStoreProductAndService.call(params[:store_id], params[:product_id])
				render json: GetOneTransformer.new.transform(store_product), status: :ok
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(STORE_PRODUCT_MODEL_NAME), status: :not_found
			end

			def create
				store_product = StoresProductsCreateService.call(params[:store_id], params[:product_id], params[:quantity])

				if store_product.errors.count == 0
					render json: CreatedTransformer.new.transform(store_product), status: :created
				else
					render json: UnprocessableEntityTransformer.new.transform(store_product.errors), status: :unprocessable_entity
				end
			rescue ActiveRecord::RecordNotUnique => e
				render json: ConflictTransformer.new.transform, status: :conflict
			end

			def update
				store_product = StoresProductsUpdateService.call(params[:store_id], params[:product_id], params[:quantity])

				if store_product.errors.count == 0
					render json: UpdatedTransformer.new.transform(store_product), status: :ok
				else
					render json: UnprocessableEntityTransformer.new.transform(store_product.errors), status: :unprocessable_entity
				end
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(STORE_PRODUCT_MODEL_NAME), status: :not_found
			end

			def delete
				store_product = StoresProductsDeleteService.call(params[:store_id], params[:product_id])
				render json: DeletedTransformer.new.transform(store_product), status: :ok
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(STORE_PRODUCT_MODEL_NAME), status: :not_found
			end
		end
	end
end
