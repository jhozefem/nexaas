module Api
	module V1
		class ProductsController < ApplicationController
			MODEL_NAME = 'Product'

			def find_all
				products = ProductsFindAllService.call
				render json: GetManyTransformer.new.transform(products), status: :ok
			end

			def find_by_id
				product = ProductsFindByIdService.call(params[:product_id])
				render json: GetOneTransformer.new.transform(product), status: :ok
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(MODEL_NAME), status: :not_found
			end

			def create
				product = ProductsCreateService.call(params[:name], params[:price])

				if product.errors.count == 0
					render json: CreatedTransformer.new.transform(product), status: :created
				else
					render json: UnprocessableEntityTransformer.new.transform(product.errors), status: :unprocessable_entity
				end
			end

			def update
				product = ProductsUpdateService.call(params[:product_id], params[:name], params[:price])

				if product.errors.count == 0
					render json: UpdatedTransformer.new.transform(product), status: :ok
				else
					render json: UnprocessableEntityTransformer.new.transform(product.errors), status: :unprocessable_entity
				end
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(MODEL_NAME), status: :not_found
			end

			def delete
				product = ProductsDeleteService.call(params[:product_id])
				render json: DeletedTransformer.new.transform(product), status: :ok
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(MODEL_NAME), status: :not_found
			end
		end
	end
end
