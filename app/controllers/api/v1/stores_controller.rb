module Api
	module V1
		class StoresController < ApplicationController
			MODEL_NAME = 'Store'

			def find_all
				stores = StoresFindAllService.call
				render json: GetManyTransformer.new.transform(stores), status: :ok
			end

			def find_by_id
				store = StoresFindByIdService.call(params[:store_id])
				render json: GetOneTransformer.new.transform(store), status: :ok
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(MODEL_NAME), status: :not_found
			end

			def create
				store = StoresCreateService.call(params[:name], params[:address])

				if store.errors.count == 0
					render json: CreatedTransformer.new.transform(store), status: :created
				else
					render json: UnprocessableEntityTransformer.new.transform(store.errors), status: :unprocessable_entity
				end
			end

			def update
				store = StoresUpdateService.call(params[:store_id], params[:name], params[:address])

				if store.errors.count == 0
					render json: UpdatedTransformer.new.transform(store), status: :ok
				else
					render json: UnprocessableEntityTransformer.new.transform(store.errors), status: :unprocessable_entity
				end
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(MODEL_NAME), status: :not_found
			end

			def delete
				store = StoresDeleteService.call(params[:store_id])
				render json: DeletedTransformer.new.transform(store), status: :ok
			rescue ActiveRecord::RecordNotFound => e
				render json: NotFoundTransformer.new.transform(MODEL_NAME), status: :not_found
			end
		end
	end
end
