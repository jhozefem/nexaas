class StoresProductsUpdateService < ApplicationService
    attr_reader :store_id, :product_id, :quantity

    def initialize(store_id, product_id, quantity)
        @store_id = store_id
        @product_id = product_id
        @quantity = quantity
    end

    def call
        store_product = StoreProduct.lock('FOR UPDATE').find_by(store_id: store_id, product_id: product_id)

        if !store_product
            raise ActiveRecord::RecordNotFound.new
        end

        if quantity != nil
            store_product.quantity = quantity
        end

        store_product.save

        return store_product
    end
end
