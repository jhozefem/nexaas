class StoresProductsFindByStoreProductAndService < ApplicationService
    attr_reader :store_id, :product_id

    def initialize(store_id, product_id)
        @store_id = store_id
        @product_id = product_id
    end

    def call
        store_product = StoreProduct.find_by(store_id: store_id, product_id: product_id)

        if !store_product
            raise ActiveRecord::RecordNotFound.new
        end

        return store_product
    end
end
