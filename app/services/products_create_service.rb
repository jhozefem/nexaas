class ProductsCreateService < ApplicationService
    attr_reader :name, :price

    def initialize(name, price)
        @name = name
        @price = price
    end

    def call
        product = Product.new()
        product.name = name
        product.price = price
        product.save

        return product
    end
end
