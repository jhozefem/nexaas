class ProductsUpdateService < ApplicationService
    attr_reader :product_id, :name, :price

    def initialize(product_id, name, price)
        @product_id = product_id
        @name = name
        @price = price
    end

    def call
        product = Product.lock('FOR UPDATE').find(product_id)

        if name != nil
            product.name = name
        end

        if price != nil
            product.price = price
        end

        product.save

        return product
    end
end
