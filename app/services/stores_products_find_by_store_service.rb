class StoresProductsFindByStoreService < ApplicationService
    attr_reader :store_id

    def initialize(store_id)
        @store_id = store_id
    end

    def call
        Store.find(store_id).store_products
    end
end
