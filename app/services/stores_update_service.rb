class StoresUpdateService < ApplicationService
    attr_reader :store_id, :name, :address

    def initialize(store_id, name, address)
        @store_id = store_id
        @name = name
        @address = address
    end

    def call
        store = Store.lock('FOR UPDATE').find(store_id)

        if name != nil
            store.name = name
        end

        if address != nil
            store.address = address
        end

        store.save

        return store
    end
end
