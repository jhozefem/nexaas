class StoresCreateService < ApplicationService
    attr_reader :name, :address

    def initialize(name, address)
        @name = name
        @address = address
    end

    def call
        store = Store.new()
        store.name = name
        store.address = address
        store.save

        return store
    end
end
