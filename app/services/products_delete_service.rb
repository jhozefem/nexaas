class ProductsDeleteService < ApplicationService
    attr_reader :product_id

    def initialize(product_id)
        @product_id = product_id
    end

    def call
        product = Product.find(product_id)
		product.destroy

        return product
    end
end
