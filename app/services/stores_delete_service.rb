class StoresDeleteService < ApplicationService
    attr_reader :store_id

    def initialize(store_id)
        @store_id = store_id
    end

    def call
        store = Store.find(store_id)
		store.destroy

        return store
    end
end
