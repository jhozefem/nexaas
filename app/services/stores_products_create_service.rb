class StoresProductsCreateService < ApplicationService
    attr_reader :store_id, :product_id, :quantity

    def initialize(store_id, product_id, quantity)
        @store_id = store_id
        @product_id = product_id
        @quantity = quantity
    end

    def call
        store_product = StoreProduct.new()
        store_product.store_id = store_id
        store_product.product_id = product_id
        store_product.quantity = quantity
        store_product.save

        return store_product
    end
end
