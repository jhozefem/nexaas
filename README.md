# Nexaas Test

## Prerequisite

* [Docker](https://www.docker.com/community-edition)
* [Docker Compose](https://docs.docker.com/compose/install)

## Up and running

First.

```console
$ make build
```

Start application:

```console
$ make up
```

Create database:

```console
$ make migrate
```

Check containers are successfully up and running:

```console
$ curl http://localhost:3000/api/health/check
```

Make sure you stop containers after usage:

```console
$ make stop
```

## Testing

Unit + Integration tests:

```console
$ make test
```

## API

### Stores

#### Get all stores

```console
$ curl http://localhost:3000/api/v1/stores
```

Example:

```
Request:
GET http://localhost:3000/api/v1/stores

Response:
{
    "total": 1,
    "data": [
        {
            "id": 1,
            "name": "Loja 1",
            "address": {
                "address1": "Rua Test, 1234",
                "address2": "Apto 1234",
                "zip_code": "12345678",
                "country": "Brasil",
                "state": "São Paulo",
                "city": "São Paulo"
            }
        }
    ]
}
```

#### Get store by id

```console
$ http://localhost:3000/api/v1/stores/:store_id
```

Example:

```
Request:
GET http://localhost:3000/api/v1/stores/1

Response:
{
    "id": 1,
    "name": "Loja 1",
    "address": {
        "address1": "Rua Test, 1234",
        "address2": "Apto 1234",
        "zip_code": "12345678",
        "country": "Brasil",
        "state": "São Paulo",
        "city": "São Paulo"
    }
}
```

#### Create stores

```console
$ curl --location --request POST 'http://localhost:3000/api/v1/stores' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "{name}",
    "address": {
        "address1": "{address1}",
        "address2": "{address2}",
        "zip_code": "{zip_code}",
        "country": "{country}",
        "state": "{state}",
        "city": "{city}"
    }
}'
```

Example:

```
Request:
POST http://localhost:3000/api/v1/stores
{
    "name": "Loja 1",
    "address": {
        "address1": "Rua Test, 1234",
        "address2": "Apto 1234",
        "zip_code": "12345678",
        "country": "Brasil",
        "state": "São Paulo",
        "city": "São Paulo"
    }
}

Response:
{
    "status": "Created",
    "data": {
        "id": 1,
        "name": "Loja 1",
        "address": {
            "address1": "Rua Test, 1234",
            "address2": "Apto 1234",
            "zip_code": "12345678",
            "country": "Brasil",
            "state": "São Paulo",
            "city": "São Paulo"
        }
    }
}
```

#### Update stores

```console
$ curl --location --request PATCH 'http://localhost:3000/api/v1/stores/:store_id' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "{name}",
    "address": {
        "address1": "{address1}",
        "address2": "{address2}",
        "zip_code": "{zip_code}",
        "country": "{country}",
        "state": "{state}",
        "city": "{city}"
    }
}'
```

Example:

```
Request:
PATCH http://localhost:3000/api/v1/stores/1
{
    "name": "Loja 2"
}

Response:
{
    "status": "Updated",
    "data": {
        "id": 1,
        "name": "Loja 2",
        "address": {
            "address1": "Rua Test, 1234",
            "address2": "Apto 1234",
            "zip_code": "12345678",
            "country": "Brasil",
            "state": "São Paulo",
            "city": "São Paulo"
        }
    }
}
```

#### Delete stores

```console
$ curl --location --request DELETE 'http://localhost:3000/api/v1/stores/:store_id'
```

Example:

```
Request:
DELETE http://localhost:3000/api/v1/stores/1

Response:
{
    "status": "Deleted",
    "data": {
        "id": 1,
        "name": "Loja 1",
        "address": {
            "address1": "Rua Test, 1234",
            "address2": "Apto 1234",
            "zip_code": "12345678",
            "country": "Brasil",
            "state": "São Paulo",
            "city": "São Paulo"
        }
    }
}
```

### Products

#### Get all products

```console
$ curl http://localhost:3000/api/v1/products
```

Example:

```
Request:
GET http://localhost:3000/api/v1/products

Response:
{
    "total": 1,
    "data": [
        {
            "id": 1,
            "name": "Produto 1",
            "price": 10.5
        }
    ]
}
```

#### Get product by id

```console
$ http://localhost:3000/api/v1/products/:product_id
```

Example:

```
Request:
GET http://localhost:3000/api/v1/products/1

Response:
{
    "id": 1,
    "name": "Produto 1",
    "price": 10.5
}
```

#### Create products

```console
$ curl --location --request POST 'http://localhost:3000/api/v1/products' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "{name}",
    "price": {price}
}'
```

Example:

```
Request:
POST http://localhost:3000/api/v1/products
{
    "name": "Produto 1",
    "price": 10.5
}

Response:
{
    "status": "Created",
    "data": {
        "id": 1,
        "name": "Produto 1",
        "price": 10.5
    }
}
```

#### Update products

```console
$ curl --location --request PATCH 'http://localhost:3000/api/v1/products/:product_id' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "{name}",
    "price": {price}
}'
```

Example:

```
Request:
PATCH http://localhost:3000/api/v1/products/1
{
    "name": "Produto 2"
}

Response:
{
    "status": "Updated",
    "data": {
        "id": 1,
        "name": "Produto 2",
        "price": 10.5
    }
}
```

#### Delete products

```console
$ curl --location --request DELETE 'http://localhost:3000/api/v1/products/:product_id'
```

Example:

```
Request:
DELETE http://localhost:3000/api/v1/products/1

Response:
{
    "status": "Deleted",
    "data": {
        "id": 1,
        "name": "Produto 1",
        "price": 10.5
    }
}
```

### Store Products

#### Get all products from store

```console
$ curl http://localhost:3000/api/v1/stores/:store_id/products
```

Example:

```
Request:
GET http://localhost:3000/api/v1/stores/1/products

Response:
{
    "total": 1,
    "data": [
        {
            "id": 1,
            "store_id": 1,
            "product_id": 1,
            "quantity": 10
        }
    ]
}
```

#### Get store product by store and product

```console
$ http://localhost:3000/api/v1/stores/:store_id/products/:product_id
```

Example:

```
Request:
GET http://localhost:3000/api/v1/stores/1/products/1

Response:
{
    "id": 1,
    "store_id": 1,
    "product_id": 1,
    "quantity": 10
}
```

#### Create store products

```console
$ curl --location --request POST 'http://localhost:3000/api/v1/stores/:store_id/products' \
--header 'Content-Type: application/json' \
--data-raw '{
    "product_id": "{product_id}",
    "quantity": {quantity}
}'
```

Example:

```
Request:
POST http://localhost:3000/api/v1/stores/1/products
{
    "product_id": 1,
    "quantity": 10
}

Response:
{
    "status": "Created",
    "data": {
        "id": 1,
        "store_id": 1,
        "product_id": 1,
        "quantity": 10
    }
}
```

#### Update store products

```console
$ curl --location --request PATCH 'http://localhost:3000/api/v1/stores/:store_id/products/:product_id' \
--header 'Content-Type: application/json' \
--data-raw '{
    "quantity": {quantity}
}'
```

Example:

```
Request:
PATCH http://localhost:3000/api/v1/stores/1/products/1
{
    "quantity": 9
}

Response:
{
    "status": "Updated",
    "data": {
        "id": 1,
        "store_id": 1,
        "product_id": 1,
        "quantity": 9
    }
}
```

#### Delete store products

```console
$ curl --location --request DELETE 'http://localhost:3000/api/v1/stores/:store_id/products/:product_id'
```

Example:

```
Request:
DELETE http://localhost:3000/api/v1/stores/1/products/1

Response:
{
    "status": "Deleted",
    "data": {
        "id": 1,
        "store_id": 1,
        "product_id": 1,
        "quantity": 10
    }
}
```
