FactoryBot.define do
    factory :random_store, class: Store do
        name { Faker::Name.name }
        address_address1 { Faker::Address.full_address }
        address_zip_code { Faker::Number.number(digits: 8) }
        address_country { Faker::Address.country }
        address_state { Faker::Address.state }
        address_city { Faker::Address.city }
    end

    factory :random_product, class: Product do
        name { Faker::Name.name }
        price { Faker::Number.decimal(l_digits: 2) }
    end

    factory :random_store_product, class: StoreProduct do
        after(:build) do |store_product, evaluator|
            if evaluator.store_id
                store_product.store_id = evaluator.store_id
            else
                store_product.store = FactoryBot.create(:random_store)
            end

            if evaluator.product_id
                store_product.product_id = evaluator.product_id
            else
                store_product.product = FactoryBot.create(:random_product)
            end

            store_product.quantity = Faker::Number.number(digits: 4)
        end
    end
end
