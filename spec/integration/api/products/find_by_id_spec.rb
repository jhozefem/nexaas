require 'rails_helper'

describe 'find product by id', :type => :request do
    context 'product not found' do
        before {get '/api/v1/products/WHATEVER'}

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "Product not found"' do
            expect(JSON.parse(response.body)['message']).to eq('Product not found')
        end
    end

    context 'product found' do
        let!(:product) {FactoryBot.create(:random_product)}

        before {get "/api/v1/products/#{product.id}"}

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        context 'validate data' do
            let!(:product_response) {JSON.parse(response.body)}

            it 'id' do
                expect(product_response['id']).to eq(product['id'])
            end

            it 'name' do
                expect(product_response['name']).to eq(product['name'])
            end

            it 'price' do
                expect(product_response['price']).to eq(product['price'])
            end
        end
    end
end
