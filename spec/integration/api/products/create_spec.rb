require 'rails_helper'

describe 'create products', :type => :request do
    context 'success' do
        before {
            post '/api/v1/products', params: {
                :name => 'WHATEVER_NAME',
                :price => 123
            }
        }

        it 'status code 201' do
            expect(response).to have_http_status(:created)
        end

        it 'status "Created"' do
            expect(JSON.parse(response.body)['status']).to eq('Created')
        end

        context 'validate data' do
            it 'id' do
                expect(JSON.parse(response.body)['data']['id']).to be_an(Numeric)
            end

            it 'name' do
                expect(JSON.parse(response.body)['data']['name']).to eq('WHATEVER_NAME')
            end

            it 'price' do
                expect(JSON.parse(response.body)['data']['price']).to eq(123)
            end
        end
    end

    context 'invalid name' do
        before {
            post '/api/v1/products', params: {
                :price => 123
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error name' do
            expect(JSON.parse(response.body)['errors']['name']).to contain_exactly("can't be blank")
        end
    end

    context 'invalid price' do
        before {
            post '/api/v1/products', params: {
                :name => 'WHATEVER_NAME'
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error price' do
            expect(JSON.parse(response.body)['errors']['price']).to contain_exactly("can't be blank", "is not a number")
        end
    end
end
