require 'rails_helper'

describe 'delete products', :type => :request do
    let!(:product) {FactoryBot.create(:random_product)}

    context 'product not found' do
        before {
            delete "/api/v1/products/WHATEVER"
        }

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "Product not found"' do
            expect(JSON.parse(response.body)['message']).to eq('Product not found')
        end
    end

    context 'success' do
        before {
            delete "/api/v1/products/#{product.id}"
        }

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'status "Deleted"' do
            expect(JSON.parse(response.body)['status']).to eq('Deleted')
        end

        context 'validate data' do
            it 'id' do
                expect(JSON.parse(response.body)['data']['id']).to eq(product['id'])
            end

            it 'name' do
                expect(JSON.parse(response.body)['data']['name']).to eq(product['name'])
            end

            it 'price' do
                expect(JSON.parse(response.body)['data']['price']).to eq(product['price'])
            end
        end
    end
end
