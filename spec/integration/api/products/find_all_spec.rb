require 'rails_helper'

describe 'find all products', :type => :request do
    context 'empty product list' do
        before {get '/api/v1/products'}

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'total equals to 0' do
            expect(JSON.parse(response.body)['total']).to eq(0)
        end

        it 'data size equals to 0' do
            expect(JSON.parse(response.body)['data'].count).to eq(0)
        end
    end

    context 'registered products' do
        let!(:products) {FactoryBot.create_list(:random_product, 20)}

        before {get '/api/v1/products'}

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'total equals to 20' do
            expect(JSON.parse(response.body)['total']).to eq(20)
        end

        it 'data size equals to 20' do
            expect(JSON.parse(response.body)['data'].count).to eq(20)
        end

        context 'validate data' do
            let!(:first_product_response) {JSON.parse(response.body)['data'][0]}

            it 'id' do
                expect(first_product_response['id']).to eq(products[0]['id'])
            end

            it 'name' do
                expect(first_product_response['name']).to eq(products[0]['name'])
            end

            it 'price' do
                expect(first_product_response['price']).to eq(products[0]['price'])
            end
        end
    end
end
