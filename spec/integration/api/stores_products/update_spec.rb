require 'rails_helper'

describe 'update store products', :type => :request do
    let!(:store_product) {FactoryBot.create(:random_store_product)}

    context 'store product not found because store doesn\'t exist' do
        before {
            patch "/api/v1/stores/WHATEVER/products/#{store_product.product_id}", params: {
                :quantity => 999
            }
        }

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "Store not found"' do
            expect(JSON.parse(response.body)['message']).to eq('StoreProduct not found')
        end
    end

    context 'store product not found because product doesn\'t exist' do
        before {
            patch "/api/v1/stores/#{store_product.store_id}/products/WHATEVER", params: {
                :quantity => 999
            }
        }

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "Store not found"' do
            expect(JSON.parse(response.body)['message']).to eq('StoreProduct not found')
        end
    end

    context 'success' do
        before {
            patch "/api/v1/stores/#{store_product.store_id}/products/#{store_product.product_id}", params: {
                :quantity => 999
            }
        }

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'status "Updated"' do
            expect(JSON.parse(response.body)['status']).to eq('Updated')
        end

        context 'validate data' do
            it 'id' do
                expect(JSON.parse(response.body)['data']['id']).to be_an(Numeric)
            end

            it 'store_id' do
                expect(JSON.parse(response.body)['data']['store_id']).to eq(store_product['store_id'])
            end

            it 'product_id' do
                expect(JSON.parse(response.body)['data']['product_id']).to eq(store_product['product_id'])
            end

            it 'quantity' do
                expect(JSON.parse(response.body)['data']['quantity']).to eq(999)
            end
        end
    end

    context 'invalid quantity' do
        before {
            patch "/api/v1/stores/#{store_product.store_id}/products/#{store_product.product_id}", params: {
                :quantity => 'abcd'
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error quantity' do
            expect(JSON.parse(response.body)['errors']['quantity']).to contain_exactly("is not a number")
        end
    end
end
