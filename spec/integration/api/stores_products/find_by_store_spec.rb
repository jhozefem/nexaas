require 'rails_helper'

describe 'find all products to store', :type => :request do
    let!(:store) {FactoryBot.create(:random_store)}

    context 'store not found' do
        before {
            get "/api/v1/stores/WHATEVER/products"
        }

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "Store not found"' do
            expect(JSON.parse(response.body)['message']).to eq('Store not found')
        end
    end

    context 'empty product list' do
        before {get "/api/v1/stores/#{store.id}/products"}

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'total equals to 0' do
            expect(JSON.parse(response.body)['total']).to eq(0)
        end

        it 'data size equals to 0' do
            expect(JSON.parse(response.body)['data'].count).to eq(0)
        end
    end

    context 'registered products to store' do
        let!(:store_products) {FactoryBot.create_list(:random_store_product, 20, store_id: store['id'])}

        before {get "/api/v1/stores/#{store.id}/products"}

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'total equals to 20' do
            expect(JSON.parse(response.body)['total']).to eq(20)
        end

        it 'data size equals to 20' do
            expect(JSON.parse(response.body)['data'].count).to eq(20)
        end

        context 'validate data' do
            let!(:first_store_product_response) {JSON.parse(response.body)['data'][0]}

            it 'id' do
                expect(first_store_product_response['id']).to eq(store_products[0]['id'])
            end

            it 'store_id' do
                expect(first_store_product_response['store_id']).to eq(store_products[0]['store_id'])
            end

            it 'product_id' do
                expect(first_store_product_response['product_id']).to eq(store_products[0]['product_id'])
            end

            it 'quantity' do
                expect(first_store_product_response['quantity']).to be_an(Numeric)
            end
        end
    end
end
