require 'rails_helper'

describe 'find store products by store and product', :type => :request do
    let!(:store) {FactoryBot.create(:random_store)}
    let!(:product) {FactoryBot.create(:random_product)}

    context 'store not found' do
        before {
            get "/api/v1/stores/WHATEVER/products"
        }

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "Store not found"' do
            expect(JSON.parse(response.body)['message']).to eq('Store not found')
        end
    end

    context 'store product not found' do
        before {get "/api/v1/stores/#{store.id}/products/#{product.id}"}

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "StoreProduct not found"' do
            expect(JSON.parse(response.body)['message']).to eq('StoreProduct not found')
        end
    end

    context 'store product found' do
        let!(:store_product) {FactoryBot.create(:random_store_product, store_id: store['id'], product_id: product['id'])}

        before {get "/api/v1/stores/#{store.id}/products/#{product.id}"}

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        context 'validate data' do
            let!(:store_product_response) {JSON.parse(response.body)}

            it 'id' do
                expect(store_product_response['id']).to eq(store_product['id'])
            end

            it 'store_id' do
                expect(store_product_response['store_id']).to eq(store_product['store_id'])
            end

            it 'product_id' do
                expect(store_product_response['product_id']).to eq(store_product['product_id'])
            end

            it 'quantity' do
                expect(store_product_response['quantity']).to eq(store_product['quantity'])
            end
        end
    end
end
