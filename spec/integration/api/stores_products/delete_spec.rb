require 'rails_helper'

describe 'delete store products', :type => :request do
    let!(:store_product) {FactoryBot.create(:random_store_product)}

    context 'store product not found because store doesn\'t exist' do
        before {
            delete "/api/v1/stores/WHATEVER/products/#{store_product.product_id}"
        }

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "StoreProduct not found"' do
            expect(JSON.parse(response.body)['message']).to eq('StoreProduct not found')
        end
    end

    context 'store product not found because product doesn\'t exist' do
        before {
            delete "/api/v1/stores/#{store_product.store_id}/products/WHATEVER"
        }

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "StoreProduct not found"' do
            expect(JSON.parse(response.body)['message']).to eq('StoreProduct not found')
        end
    end

    context 'success' do
        before {
            delete "/api/v1/stores/#{store_product.store_id}/products/#{store_product.product_id}"
        }

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'status "Deleted"' do
            expect(JSON.parse(response.body)['status']).to eq('Deleted')
        end

        context 'validate data' do
            it 'id' do
                expect(JSON.parse(response.body)['data']['id']).to be_an(Numeric)
            end

            it 'store_id' do
                expect(JSON.parse(response.body)['data']['store_id']).to eq(store_product['store_id'])
            end

            it 'product_id' do
                expect(JSON.parse(response.body)['data']['product_id']).to eq(store_product['product_id'])
            end

            it 'quantity' do
                expect(JSON.parse(response.body)['data']['quantity']).to be_an(Numeric)
            end
        end
    end
end
