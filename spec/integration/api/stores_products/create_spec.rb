require 'rails_helper'

describe 'create store products', :type => :request do
    let!(:store) {FactoryBot.create(:random_store)}
    let!(:product) {FactoryBot.create(:random_product)}

    context 'success' do
        before {
            post "/api/v1/stores/#{store.id}/products", params: {
                :product_id => product['id'],
                :quantity => 10
            }
        }

        it 'status code 201' do
            expect(response).to have_http_status(:created)
        end

        it 'status "Created"' do
            expect(JSON.parse(response.body)['status']).to eq('Created')
        end

        context 'validate data' do
            it 'id' do
                expect(JSON.parse(response.body)['data']['id']).to be_an(Numeric)
            end

            it 'store_id' do
                expect(JSON.parse(response.body)['data']['store_id']).to eq(store['id'])
            end

            it 'product_id' do
                expect(JSON.parse(response.body)['data']['product_id']).to eq(product['id'])
            end

            it 'quantity' do
                expect(JSON.parse(response.body)['data']['quantity']).to eq(10)
            end
        end
    end

    context 'store product already exists' do
        let!(:store_product) {FactoryBot.create(:random_store_product)}

        before {
            post "/api/v1/stores/#{store_product.store_id}/products", params: {
                :product_id => store_product['product_id'],
                :quantity => 10
            }
        }

        it 'status code 409' do
            expect(response).to have_http_status(:conflict)
        end

        it 'status "ConflictError"' do
            expect(JSON.parse(response.body)['status']).to eq('ConflictError')
        end

        it 'message "Item already exists"' do
            expect(JSON.parse(response.body)['message']).to eq('Item already exists')
        end
    end

    context 'invalid store' do
        before {
            post '/api/v1/stores/WHATEVER/products', params: {
                :product_id => product['id'],
                :quantity => 10
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error store_id' do
            expect(JSON.parse(response.body)['errors']['store']).to contain_exactly("must exist")
        end
    end

    context 'invalid product' do
        before {
            post "/api/v1/stores/#{store.id}/products", params: {
                :product_id => 'WHATEVER',
                :quantity => 10
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error product_id' do
            expect(JSON.parse(response.body)['errors']['product']).to contain_exactly("must exist")
        end
    end

    context 'invalid quantity' do
        before {
            post "/api/v1/stores/#{store.id}/products", params: {
                :product_id => product['id'],
                :quantity => 'abcd'
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error quantity' do
            expect(JSON.parse(response.body)['errors']['quantity']).to contain_exactly("is not a number")
        end
    end
end
