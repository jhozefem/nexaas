require 'rails_helper'

describe 'create stores', :type => :request do
    context 'success' do
        before {
            post '/api/v1/stores', params: {
                :name => 'WHATEVER_NAME',
                :address => {
                    :address1 => 'WHATEVER_ADDRESS1',
                    :address2 => 'WHATEVER_ADDRESS2',
                    :zip_code => '12345678',
                    :country => 'WHATEVER_COUNTRY',
                    :state => 'WHATEVER_STATE',
                    :city => 'WHATEVER_CITY'
                }
            }
        }

        it 'status code 201' do
            expect(response).to have_http_status(:created)
        end

        it 'status "Created"' do
            expect(JSON.parse(response.body)['status']).to eq('Created')
        end

        context 'validate data' do
            it 'id' do
                expect(JSON.parse(response.body)['data']['id']).to be_an(Numeric)
            end

            it 'name' do
                expect(JSON.parse(response.body)['data']['name']).to eq('WHATEVER_NAME')
            end

            it 'address' do
                expect(JSON.parse(response.body)['data']['address']).to eq({
                    "address1" => 'WHATEVER_ADDRESS1',
                    "address2" => 'WHATEVER_ADDRESS2',
                    "zip_code" => '12345678',
                    "country" => 'WHATEVER_COUNTRY',
                    "state" => 'WHATEVER_STATE',
                    "city" => 'WHATEVER_CITY'
                })
            end
        end
    end

    context 'invalid name' do
        before {
            post '/api/v1/stores', params: {
                :address => {
                    :address1 => 'WHATEVER_ADDRESS1',
                    :address2 => 'WHATEVER_ADDRESS2',
                    :zip_code => '12345678',
                    :country => 'WHATEVER_COUNTRY',
                    :state => 'WHATEVER_STATE',
                    :city => 'WHATEVER_CITY'
                }
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error name' do
            expect(JSON.parse(response.body)['errors']['name']).to contain_exactly("can't be blank")
        end
    end

    context 'empty address' do
        before {
            post '/api/v1/stores', params: {
                :name => 'WHATEVER_NAME'
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error address' do
            expect(JSON.parse(response.body)['errors']['address_address1']).to contain_exactly("can't be blank")
            expect(JSON.parse(response.body)['errors']['address_zip_code']).to contain_exactly("can't be blank", "must contain 8 digits")
            expect(JSON.parse(response.body)['errors']['address_country']).to contain_exactly("can't be blank")
            expect(JSON.parse(response.body)['errors']['address_state']).to contain_exactly("can't be blank")
            expect(JSON.parse(response.body)['errors']['address_city']).to contain_exactly("can't be blank")
        end
    end

    context 'invalid address zip_code with letters' do
        before {
            post '/api/v1/stores', params: {
                :name => 'WHATEVER_NAME',
                :address => {
                    :zip_code => 'abcdefgh'
                }
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error address' do
            expect(JSON.parse(response.body)['errors']['address_zip_code']).to contain_exactly("must contain 8 digits")
        end
    end

    context 'invalid address zip_code wrong digits quantity' do
        before {
            post '/api/v1/stores', params: {
                :name => 'WHATEVER_NAME',
                :address => {
                    :zip_code => '1234567'
                }
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error address' do
            expect(JSON.parse(response.body)['errors']['address_zip_code']).to contain_exactly("must contain 8 digits")
        end
    end
end
