require 'rails_helper'

describe 'find all stores', :type => :request do
    context 'empty store list' do
        before {get '/api/v1/stores'}

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'total equals to 0' do
            expect(JSON.parse(response.body)['total']).to eq(0)
        end

        it 'data size equals to 0' do
            expect(JSON.parse(response.body)['data'].count).to eq(0)
        end
    end

    context 'registered stores' do
        let!(:stores) {FactoryBot.create_list(:random_store, 20)}

        before {get '/api/v1/stores'}

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'total equals to 20' do
            expect(JSON.parse(response.body)['total']).to eq(20)
        end

        it 'data size equals to 20' do
            expect(JSON.parse(response.body)['data'].count).to eq(20)
        end

        context 'validate data' do
            let!(:first_store_response) {JSON.parse(response.body)['data'][0]}

            it 'id' do
                expect(first_store_response['id']).to eq(stores[0]['id'])
            end

            it 'name' do
                expect(first_store_response['name']).to eq(stores[0]['name'])
            end

            context 'address section' do
                it 'address1' do
                    expect(first_store_response['address_address1']).to be_nil
                    expect(first_store_response['address']['address1']).to eq(stores[0]['address_address1'])
                end

                it 'address2' do
                    expect(first_store_response['address_address2']).to be_nil
                    expect(first_store_response['address']['address2']).to eq(stores[0]['address_address2'])
                end

                it 'zip_code' do
                    expect(first_store_response['address_zip_code']).to be_nil
                    expect(first_store_response['address']['zip_code']).to eq(stores[0]['address_zip_code'])
                end

                it 'country' do
                    expect(first_store_response['address_country']).to be_nil
                    expect(first_store_response['address']['country']).to eq(stores[0]['address_country'])
                end

                it 'state' do
                    expect(first_store_response['address_state']).to be_nil
                    expect(first_store_response['address']['state']).to eq(stores[0]['address_state'])
                end

                it 'city' do
                    expect(first_store_response['address_city']).to be_nil
                    expect(first_store_response['address']['city']).to eq(stores[0]['address_city'])
                end
            end
        end
    end
end
