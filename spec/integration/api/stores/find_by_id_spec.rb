require 'rails_helper'

describe 'find store by id', :type => :request do
    context 'store not found' do
        before {get '/api/v1/stores/WHATEVER'}

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "Store not found"' do
            expect(JSON.parse(response.body)['message']).to eq('Store not found')
        end
    end

    context 'store found' do
        let!(:store) {FactoryBot.create(:random_store)}

        before {get "/api/v1/stores/#{store.id}"}

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        context 'validate data' do
            let!(:store_response) {JSON.parse(response.body)}

            it 'id' do
                expect(store_response['id']).to eq(store['id'])
            end

            it 'name' do
                expect(store_response['name']).to eq(store['name'])
            end

            context 'address section' do
                it 'address1' do
                    expect(store_response['address_address1']).to be_nil
                    expect(store_response['address']['address1']).to eq(store['address_address1'])
                end

                it 'address2' do
                    expect(store_response['address_address2']).to be_nil
                    expect(store_response['address']['address2']).to eq(store['address_address2'])
                end

                it 'zip_code' do
                    expect(store_response['address_zip_code']).to be_nil
                    expect(store_response['address']['zip_code']).to eq(store['address_zip_code'])
                end

                it 'country' do
                    expect(store_response['address_country']).to be_nil
                    expect(store_response['address']['country']).to eq(store['address_country'])
                end

                it 'state' do
                    expect(store_response['address_state']).to be_nil
                    expect(store_response['address']['state']).to eq(store['address_state'])
                end

                it 'city' do
                    expect(store_response['address_city']).to be_nil
                    expect(store_response['address']['city']).to eq(store['address_city'])
                end
            end
        end
    end
end
