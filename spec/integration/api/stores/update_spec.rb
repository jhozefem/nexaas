require 'rails_helper'

describe 'update stores', :type => :request do
    let!(:store) {FactoryBot.create(:random_store)}

    context 'store not found' do
        before {
            patch "/api/v1/stores/WHATEVER", params: {
                :name => 'WHATEVER_NAME',
                :address => {
                    :address1 => 'WHATEVER_ADDRESS1',
                    :address2 => 'WHATEVER_ADDRESS2',
                    :zip_code => '12345678',
                    :country => 'WHATEVER_COUNTRY',
                    :state => 'WHATEVER_STATE',
                    :city => 'WHATEVER_CITY'
                }
            }
        }

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "Store not found"' do
            expect(JSON.parse(response.body)['message']).to eq('Store not found')
        end
    end

    context 'success' do
        before {
            patch "/api/v1/stores/#{store.id}", params: {
                :name => 'WHATEVER_NAME',
                :address => {
                    :address1 => 'WHATEVER_ADDRESS1',
                    :address2 => 'WHATEVER_ADDRESS2',
                    :zip_code => '12345678',
                    :country => 'WHATEVER_COUNTRY',
                    :state => 'WHATEVER_STATE',
                    :city => 'WHATEVER_CITY'
                }
            }
        }

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'status "Updated"' do
            expect(JSON.parse(response.body)['status']).to eq('Updated')
        end

        context 'validate data' do
            it 'id' do
                expect(JSON.parse(response.body)['data']['id']).to eq(store['id'])
            end

            it 'name' do
                expect(JSON.parse(response.body)['data']['name']).to eq('WHATEVER_NAME')
            end

            it 'address' do
                expect(JSON.parse(response.body)['data']['address']).to eq({
                    "address1" => 'WHATEVER_ADDRESS1',
                    "address2" => 'WHATEVER_ADDRESS2',
                    "zip_code" => '12345678',
                    "country" => 'WHATEVER_COUNTRY',
                    "state" => 'WHATEVER_STATE',
                    "city" => 'WHATEVER_CITY'
                })
            end
        end
    end

    context 'invalid address zip_code with letters' do
        before {
            patch "/api/v1/stores/#{store.id}", params: {
                :address => {
                    :zip_code => 'abcdefgh'
                }
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error address' do
            expect(JSON.parse(response.body)['errors']['address_zip_code']).to contain_exactly("must contain 8 digits")
        end
    end

    context 'invalid address zip_code wrong digits quantity' do
        before {
            patch "/api/v1/stores/#{store.id}", params: {
                :address => {
                    :zip_code => '1234567'
                }
            }
        }

        it 'status code 422' do
            expect(response).to have_http_status(:unprocessable_entity)
        end

        it 'status "UnprocessableEntityError"' do
            expect(JSON.parse(response.body)['status']).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(JSON.parse(response.body)['message']).to eq('Invalid data')
        end

        it 'error address' do
            expect(JSON.parse(response.body)['errors']['address_zip_code']).to contain_exactly("must contain 8 digits")
        end
    end
end
