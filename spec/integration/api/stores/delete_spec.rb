require 'rails_helper'

describe 'delete stores', :type => :request do
    let!(:store) {FactoryBot.create(:random_store)}

    context 'store not found' do
        before {
            delete "/api/v1/stores/WHATEVER"
        }

        it 'status code 404' do
            expect(response).to have_http_status(:not_found)
        end

        it 'status "NotFoundError"' do
            expect(JSON.parse(response.body)['status']).to eq('NotFoundError')
        end

        it 'message "Store not found"' do
            expect(JSON.parse(response.body)['message']).to eq('Store not found')
        end
    end

    context 'success' do
        before {
            delete "/api/v1/stores/#{store.id}"
        }

        it 'status code 200' do
            expect(response).to have_http_status(:ok)
        end

        it 'status "Deleted"' do
            expect(JSON.parse(response.body)['status']).to eq('Deleted')
        end

        context 'validate data' do
            it 'id' do
                expect(JSON.parse(response.body)['data']['id']).to eq(store['id'])
            end

            it 'name' do
                expect(JSON.parse(response.body)['data']['name']).to eq(store['name'])
            end

            it 'address' do
                expect(JSON.parse(response.body)['data']['address']).to eq({
                    "address1" => store['address_address1'],
                    "address2" => store['address_address2'],
                    "zip_code" => store['address_zip_code'],
                    "country" => store['address_country'],
                    "state" => store['address_state'],
                    "city" => store['address_city']
                })
            end
        end
    end
end
