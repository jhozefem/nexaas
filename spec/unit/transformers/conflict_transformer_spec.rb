require 'rails_helper'

describe 'conflict transformer', :type => :request do
    let!(:response) {ConflictTransformer.new.transform}

    it 'status "ConflictError"' do
        expect(response[:status]).to eq('ConflictError')
    end

    it 'message "Item already exists"' do
        expect(response[:message]).to eq('Item already exists')
    end
end
