require 'rails_helper'

describe 'get many transformer', :type => :request do
    let!(:response) do
        itemsMock = []

        3.times do |i|
            itemMock = Object.new
            itemMock.stub_chain('to_json').and_return({ 'index': i })
            itemsMock.append(itemMock)
        end

        GetManyTransformer.new.transform(itemsMock)
    end

    it 'total' do
        expect(response[:total]).to eq(3)
    end

    it 'check first item' do
        expect(response[:data][0]).to eq({ 'index': 0 })
    end

    it 'check second item' do
        expect(response[:data][1]).to eq({ 'index': 1 })
    end

    it 'check third item' do
        expect(response[:data][2]).to eq({ 'index': 2 })
    end
end
