require 'rails_helper'

describe 'unprocessable entity transformer', :type => :request do
    context 'nil errors' do
        let!(:response) {UnprocessableEntityTransformer.new.transform}

        it 'status "UnprocessableEntityError"' do
            expect(response[:status]).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(response[:message]).to eq('Invalid data')
        end

        it 'errors' do
            expect(response[:errors]).to eq(nil)
        end
    end

    context 'empty errors' do
        let!(:response) {UnprocessableEntityTransformer.new.transform([])}

        it 'status "UnprocessableEntityError"' do
            expect(response[:status]).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(response[:message]).to eq('Invalid data')
        end

        it 'errors' do
            expect(response[:errors]).to be_empty
        end
    end

    context 'error list' do
        let!(:response) {UnprocessableEntityTransformer.new.transform([ 'field' => 'error' ])}

        it 'status "UnprocessableEntityError"' do
            expect(response[:status]).to eq('UnprocessableEntityError')
        end

        it 'message "Invalid data"' do
            expect(response[:message]).to eq('Invalid data')
        end

        it 'errors' do
            expect(response[:errors]).to eq([ 'field' => 'error' ])
        end
    end
end
