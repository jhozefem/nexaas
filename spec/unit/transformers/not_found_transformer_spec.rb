require 'rails_helper'

describe 'not found transformer', :type => :request do
    context 'nil item' do
        let!(:response) {NotFoundTransformer.new.transform}

        it 'status "NotFoundError"' do
            expect(response[:status]).to eq('NotFoundError')
        end

        it 'message' do
            expect(response[:message]).to eq('Item not found')
        end
    end

    context 'filled item' do
        let!(:response) {NotFoundTransformer.new.transform('WHATEVER')}

        it 'status "NotFoundError"' do
            expect(response[:status]).to eq('NotFoundError')
        end

        it 'message' do
            expect(response[:message]).to eq('WHATEVER not found')
        end
    end
end
