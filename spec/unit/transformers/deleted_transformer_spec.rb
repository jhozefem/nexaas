require 'rails_helper'

describe 'deleted transformer', :type => :request do
    let!(:response) do
        itemMock = Object.new
        itemMock.stub_chain('to_json').and_return({ 'a': 'b' })

        DeletedTransformer.new.transform(itemMock)
    end

    it 'status "Deleted"' do
        expect(response[:status]).to eq('Deleted')
    end

    it 'data' do
        expect(response[:data]).to eq({ 'a': 'b' })
    end
end
