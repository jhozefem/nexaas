require 'rails_helper'

describe 'created transformer', :type => :request do
    let!(:response) do
        itemMock = Object.new
        itemMock.stub_chain('to_json').and_return({ 'a': 'b' })

        CreatedTransformer.new.transform(itemMock)
    end

    it 'status "Created"' do
        expect(response[:status]).to eq('Created')
    end

    it 'data' do
        expect(response[:data]).to eq({ 'a': 'b' })
    end
end
