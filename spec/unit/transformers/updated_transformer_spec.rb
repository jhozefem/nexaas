require 'rails_helper'

describe 'updated transformer', :type => :request do
    let!(:response) do
        itemMock = Object.new
        itemMock.stub_chain('to_json').and_return({ 'a': 'b' })

        UpdatedTransformer.new.transform(itemMock)
    end

    it 'status "Updated"' do
        expect(response[:status]).to eq('Updated')
    end

    it 'data' do
        expect(response[:data]).to eq({ 'a': 'b' })
    end
end
