require 'rails_helper'

describe 'get one transformer', :type => :request do
    let!(:response) do
        itemMock = Object.new
        itemMock.stub_chain('to_json').and_return({ 'a': 'b' })

        GetOneTransformer.new.transform(itemMock)
    end

    it 'response' do
        expect(response).to eq({ 'a': 'b' })
    end
end
