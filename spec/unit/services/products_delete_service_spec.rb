require 'rails_helper'

describe 'products delete service', :type => :request do
    let!(:productMock) do
        mock = Product.new
        mock.stub_chain('destroy').and_return(true)

        return mock
    end

    it 'should find and destroy methods called' do
        expect(Product).to receive(:find).with(1234).and_return(productMock)
        expect(productMock).to receive(:destroy).with(no_args).and_return(true)

        ProductsDeleteService.call(1234)
    end
end
