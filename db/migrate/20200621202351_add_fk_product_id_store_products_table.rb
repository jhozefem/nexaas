class AddFkProductIdStoreProductsTable < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :store_products, :products
  end
end
