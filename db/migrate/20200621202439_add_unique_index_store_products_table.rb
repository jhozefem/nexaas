class AddUniqueIndexStoreProductsTable < ActiveRecord::Migration[5.2]
  def change
    add_index :store_products, [:store_id, :product_id], unique: true
  end
end
