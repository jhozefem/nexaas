class CreateStores < ActiveRecord::Migration[5.2]
  def change
    create_table :stores do |t|
      t.string :name
      t.string :address_address1
      t.string :address_address2
      t.string :address_zip_code
      t.string :address_country
      t.string :address_state
      t.string :address_city

      t.timestamps
    end
  end
end
