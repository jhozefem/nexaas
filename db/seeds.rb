# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Store.create([
    {
        name: 'Store 01'
    },
    {
        name: 'Store 02'
    },
    {
        name: 'Store 03'
    },
    {
        name: 'Store 04'
    },
    {
        name: 'Store 05'
    }
])

Product.create([
    {
        name: 'Product 01',
        price: 11.50
    },
    {
        name: 'Product 02',
        price: 12.50
    },
    {
        name: 'Product 03',
        price: 13.50
    },
    {
        name: 'Product 04',
        price: 14.50
    },
    {
        name: 'Product 05',
        price: 15.50
    }
])
